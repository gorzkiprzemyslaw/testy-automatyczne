package SOWA.Test;

import SOWA.CreateProjectPageObject;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;

public class CreateProjectTest extends CreateProjectPageObject {

    @BeforeMethod
    public void setUpLogin() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/login");

    }

    @Test
    public void createApplication(){
        String login  = "przemek@automat.pl";
        String password = "123Zaloguj!WSXC";
        LoginTest loginTest = new LoginTest();
        loginTest.fillForm(login ,password);

        CreateProjectPageObject createProjectPageObject = new CreateProjectPageObject();
        createProjectPageObject.createProject();
        sleep(5000);
    }
}
