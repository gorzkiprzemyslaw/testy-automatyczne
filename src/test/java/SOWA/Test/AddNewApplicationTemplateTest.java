package SOWA.Test;

import SOWA.AddNewApplicationTemplatePageObject;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;

public class AddNewApplicationTemplateTest extends AddNewApplicationTemplatePageObject {

    @BeforeMethod
    public void setUpLogin() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/login");

    }

    @Test
    public void AddNewApplicationTemplate (){

        AddNewApplicationTemplatePageObject addNewApplicationTemplatePageObject = new AddNewApplicationTemplatePageObject();

        LoginTest loginTest = new LoginTest();
        String login  = "instadm1@test.pl";
        String password = "1qaz2wsx#EDC";
        loginTest.fillForm(login ,password);


        addNewApplicationTemplatePageObject.fillForm();
        sleep(5000);
    }
}
