package SOWA.Test.Sections;

import SOWA.Sections.EditApplicationPageObject;
import SOWA.Sections.TasksPageObject;
import SOWA.Test.LoginTest;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class TasksTest {

    @BeforeTest
    public void setUpLogin() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/login");
    }

    @Test
    public void tasks (){

        String login = "przemek@automat.pl";
        String password = "123Zaloguj!WSXC";
        LoginTest loginTest = new LoginTest();
        loginTest.fillForm(login, password);

        EditApplicationPageObject editApplicationPageObject = new EditApplicationPageObject();
        TasksPageObject tasksPageObject = new TasksPageObject();

        editApplicationPageObject.editApplication();
        tasksPageObject.editSection();
    }
}
