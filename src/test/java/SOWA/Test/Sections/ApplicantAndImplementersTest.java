package SOWA.Test.Sections;

import SOWA.Sections.ApplicantAndImplementersPageObject;
import SOWA.Sections.EditApplicationPageObject;
import SOWA.Test.LoginTest;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class ApplicantAndImplementersTest {

    @BeforeTest
    public void setUpLogin() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/login");
    }

    @Test
    public void applicantAndImplementers (){

        String login = "przemek@automat.pl";
        String password = "123Zaloguj!WSXC";
        LoginTest loginTest = new LoginTest();
        loginTest.fillForm(login, password);

        EditApplicationPageObject editApplicationPageObject = new EditApplicationPageObject();
        ApplicantAndImplementersPageObject applicantAndImplementersPageObject = new ApplicantAndImplementersPageObject();

        editApplicationPageObject.editApplication();
        applicantAndImplementersPageObject.editSection();

    }
}
