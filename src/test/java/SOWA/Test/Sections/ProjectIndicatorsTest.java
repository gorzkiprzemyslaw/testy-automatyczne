package SOWA.Test.Sections;

import SOWA.Sections.EditApplicationPageObject;
import SOWA.Sections.ProjectIndicatorsPageObject;
import SOWA.Test.LoginTest;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class ProjectIndicatorsTest {

    @BeforeTest
    public void setUpLogin() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/login");
    }

    @Test
    public void projectIndicators (){

        String login = "przemek@automat.pl";
        String password = "123Zaloguj!WSXC";
        LoginTest loginTest = new LoginTest();
        loginTest.fillForm(login, password);

        EditApplicationPageObject editApplicationPageObject = new EditApplicationPageObject();
        ProjectIndicatorsPageObject projectIndicatorsPageObject = new ProjectIndicatorsPageObject();

        editApplicationPageObject.editApplication();
        projectIndicatorsPageObject.editSection();
    }
}
