package SOWA.Test;

import SOWA.RegisterPageObject;
import SOWA.SetupRegister;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;


public class RegisterTest extends SetupRegister {

    @Test
    public void fillForm() {

        String email = "przemyslaw.gorzki@britenet.com.pl";
        String password = "Zaq-1234!54321";
        String name = "Temporaryname";
        String pesel = "37041733369";
        String phone = "222222222";

        RegisterPageObject registerPageObject = new RegisterPageObject();

        registerPageObject.inputEmail(email);
        registerPageObject.repeatEmailInput(email);
        registerPageObject.passwordInput(password);
        registerPageObject.repeatPasswordInput(password);
        registerPageObject.nameInput(name);
        registerPageObject.sureNameInput(name);
        registerPageObject.peselInput(pesel);
        registerPageObject.phoneInput(phone);
        registerPageObject.clickRegulations();
        registerPageObject.scrollRegulations();
        registerPageObject.acceptRegulations();
        registerPageObject.register();
        sleep(5000);

    }



}
