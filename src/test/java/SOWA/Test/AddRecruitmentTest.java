package SOWA.Test;

import SOWA.AddRecruitmentPageObject;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class AddRecruitmentTest  {

    @BeforeMethod
    public void setUpLogin() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/login");

    }

    @Test
    public void addRecruitment() {

        AddRecruitmentPageObject addRecruitmentPageObject = new AddRecruitmentPageObject();

        LoginTest loginTest = new LoginTest();
        String login  = "instadm1@test.pl";
        String password = "1qaz2wsx#EDC";
        loginTest.fillForm(login ,password);

        addRecruitmentPageObject.recruitmentList();
        addRecruitmentPageObject.addNewRecruitment();
        addRecruitmentPageObject.choosePWD();
        addRecruitmentPageObject.inputPWD();
        addRecruitmentPageObject.search();
        addRecruitmentPageObject.checkboxPWD();
        addRecruitmentPageObject.chooseButton();
        addRecruitmentPageObject.selectApplicantType();
        addRecruitmentPageObject.chooseCompanySize();
        addRecruitmentPageObject.enterBudget();
        addRecruitmentPageObject.description();
        addRecruitmentPageObject.setWebsite();
        addRecruitmentPageObject.setRecruitmentMode();
        addRecruitmentPageObject.addRound();
        addRecruitmentPageObject.setStartDate();
        addRecruitmentPageObject.setEndDate();
       // addRecruitmentPageObject.selectInstytutionEmployees();
        addRecruitmentPageObject.addAnyEmail();
        addRecruitmentPageObject.createNewRecruitment();
        addRecruitmentPageObject.setRecruitmentNumber();



    }



}
