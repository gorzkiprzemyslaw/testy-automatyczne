package SOWA.Test;

import SOWA.LoginPageObject;
import SOWA.SetupLogin;
import org.testng.annotations.Test;
import java.util.concurrent.ThreadLocalRandom;
import static com.codeborne.selenide.Selenide.screenshot;
import static com.codeborne.selenide.Selenide.sleep;


public class LoginTest extends SetupLogin {

    @Test
    public void fillForm(String login, String password){
        int int_random = ThreadLocalRandom.current().nextInt();


        LoginPageObject loginPageObject = new LoginPageObject();

        loginPageObject.loginInput(login);
        loginPageObject.passwordInput(password);
        loginPageObject.clickLoginButton();
        sleep(10000);
    }

}
