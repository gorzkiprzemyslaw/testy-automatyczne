package SOWA.Test;

import SOWA.ConfirmRecruitmentPageObject;
import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;

public class ConfirmRecruitmentTest {

    @BeforeTest
    public void setUpLogin() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/login");
    }

    @Test
    public void confirmRecruitmentTest(){
        ConfirmRecruitmentPageObject confirmRecruitmentPageObject = new ConfirmRecruitmentPageObject();
        LoginTest loginTest = new LoginTest();
        String login  = "instadm1@test.pl";
        String password = "1qaz2wsx#EDC";
        loginTest.fillForm(login ,password);

        confirmRecruitmentPageObject.addApplicationToRecruitment();
        sleep(3000);
        confirmRecruitmentPageObject.confirmRecruitment();
    }


}
