package SOWA;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Driver;
import org.testng.annotations.BeforeMethod;


import static com.codeborne.selenide.Selenide.open;

public class SetupRegister {

    @BeforeMethod
    public void setUpRegister() {

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        open("https://sowatest.britenet.dev/no-auth/register");

    }
}
