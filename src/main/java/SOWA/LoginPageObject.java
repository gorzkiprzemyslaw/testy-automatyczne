package SOWA;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selectors.byId;

public class LoginPageObject {

    private final SelenideElement loginInput = $(byId("mat-input-0"));
    private final SelenideElement passwordInput = $(byId("mat-input-1"));
    private final SelenideElement logInButton = $(byXpath("/html/body/app-root/app-main-view/div/mat-sidenav-container/mat-sidenav-content/div/main/app-login-view/div/div/div[1]/app-login/div[2]/form/div[2]/div/button"));

    public void loginInput(String login) {
        loginInput.sendKeys(login);
    }

    public void passwordInput(String password) {
        passwordInput.sendKeys(password);
    }

    public void clickLoginButton() {
        logInButton.click();
    }
}
