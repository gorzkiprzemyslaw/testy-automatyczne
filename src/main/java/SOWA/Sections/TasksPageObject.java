package SOWA.Sections;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class TasksPageObject {

    private final SelenideElement topBar = $(byXpath("//mat-tab-group/mat-tab-header/div[2]/div/div/div[4]"));
    private final SelenideElement editSection = $(byXpath("//section[1]/div/app-application-section-actions/div/div/button"));
    private final SelenideElement addTask = $(byXpath("//app-tasks-input/div[2]/button"));
    private final SelenideElement normalTask = $(byXpath("//div[2]/div/div/div/button[1]"));
    private final SelenideElement taskName = $(byXpath("//div[1]/mat-form-field/div/div[1]/div/textarea"));
    private final SelenideElement startDate = $(byXpath("//div[3]/div[2]/mat-form-field/div/div[1]/div[1]/input"));
    private final SelenideElement endDate = $(byXpath("//div[3]/div[3]/mat-form-field/div/div[1]/div[1]/input"));
    private final SelenideElement taskDescription = $(byXpath("//div[4]/mat-form-field/div/div[1]/div/textarea"));

    private final SelenideElement save = $(byXpath("//app-application-tasks-section/div/app-application-section-actions/div/div[2]/button[1]"));
    private final SelenideElement submit = $(byXpath("//app-application-tasks-section/div/app-application-section-actions/div/div[1]/button[1]"));
    private final SelenideElement saveNotification = $(byXpath("//app-notification/div[2]/div/div/p"));
    private final SelenideElement submitNotification = $(byXpath("//app-notification/div[2]/div/div/p"));

    public void editSection (){
        topBar.click();
        editSection.click();
        addTask.click();
        normalTask.click();
        taskName.sendKeys("Test task");
        startDate.sendKeys("2021-03-13");
        endDate.sendKeys("2021-03-18");
        taskDescription.sendKeys("Test test");

        save.shouldBe(Condition.visible).click();
        sleep(2000);
        String info1 = saveNotification.shouldBe(Condition.visible).getText();
        System.out.print("Sekcja zadania. " + info1);
        sleep(2000);
        submit.shouldBe(Condition.visible).click();
        sleep(2000);
        String info2 = submitNotification.shouldBe(Condition.visible).getText();
        System.out.print(info2);
    }

}
