package SOWA.Sections;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ProjectIndicatorsPageObject {

    private final SelenideElement topBar = $(byXpath("//mat-tab-group/mat-tab-header/div[2]/div/div/div[3]"));
    private final SelenideElement editSection = $(byXpath("//section[1]/div/app-application-section-actions/div/div/button"));
    private final SelenideElement addProductIndicator = $(byXpath("//app-product-indicator-input/div/div[2]/button"));
    private final SelenideElement indicatorType = $(byXpath("//div[2]/div/div/div/button[1]"));
    private final SelenideElement projectImplementationRate = $(byXpath("//mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement projectImplementationRateOption = $(byXpath("//mat-option[7]"));
    private final SelenideElement targetValueWomen = $(byXpath("//div[5]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement targetValueMen = $(byXpath("//div[6]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement indicatorDescription = $(byXpath("//textarea"));

    private final SelenideElement addResultIndicator = $(byXpath("//app-result-indicator-input/div/div[2]/button"));
    private final SelenideElement projectImplementationRate2 = $(byXpath("//app-result-indicator-input/div/div[1]/div[3]/div[2]/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement projectImplementationRateOption2 = $(byXpath("//mat-option[1]"));
    private final SelenideElement totalTargetValue = $(byXpath("//div[7]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement indicatorDescription2 = $(byXpath("//app-result-indicator-input/div/div[1]/div[3]/div[8]/mat-form-field/div/div[1]/div/textarea"));

    private final SelenideElement save = $(byXpath("//app-application-project-indicators-section/div[2]/app-application-section-actions/div/div[2]/button[1]"));
    private final SelenideElement submit = $(byXpath("//app-application-project-indicators-section/div[2]/app-application-section-actions/div/div[1]/button[1]"));
    private final SelenideElement saveNotification = $(byXpath("//app-notification/div[2]/div/div/p"));
    private final SelenideElement submitNotification = $(byXpath("//app-notification/div[2]/div/div/p"));

    public void editSection (){
        topBar.click();
        editSection.click();
        addProductIndicator.click();
        indicatorType.click();
        projectImplementationRate.click();
        projectImplementationRateOption.click();
        targetValueWomen.sendKeys("200");
        targetValueMen.sendKeys("200");
        indicatorDescription.sendKeys("Test test");

        addResultIndicator.click();
        indicatorType.click();
        projectImplementationRate2.click();
        projectImplementationRateOption2.click();
        totalTargetValue.sendKeys("500");
        indicatorDescription2.sendKeys("Test test");

        save.shouldBe(Condition.visible).click();
        sleep(2000);
        String info1 = saveNotification.shouldBe(Condition.visible).getText();
        System.out.print("Sekcja wskaźniki projektu. " + info1);
        sleep(2000);
        submit.shouldBe(Condition.visible).click();
        sleep(2000);
        String info2 = submitNotification.shouldBe(Condition.visible).getText();
        System.out.print(info2);

    }


}
