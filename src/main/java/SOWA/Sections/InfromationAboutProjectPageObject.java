package SOWA.Sections;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class InfromationAboutProjectPageObject {

    private final SelenideElement topBar = $(byXpath("//mat-tab-group/mat-tab-header/div[2]/div/div/div[1]"));
    private final SelenideElement editSection = $(byXpath("//app-application-project-information-section/section[1]/div/app-application-section-actions/div/div/button"));
    private final SelenideElement dziedzinaProjektu = $(byXpath("//div[2]/div/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement option = $(byXpath("//mat-option[2]"));
    private final SelenideElement startDateInput = $(byXpath("//div[1]/mat-form-field/div/div[1]/div[1]/input"));
    private final SelenideElement endDateInput = $(byXpath("//div[2]/mat-form-field/div/div[1]/div[1]/input"));
    private final SelenideElement projectDescription = $(byXpath("//div[4]/mat-form-field/div/div[1]/div/textarea"));
    private final SelenideElement grupyDocelowe = $(byXpath("//div[5]/mat-form-field/div/div[1]/div/textarea"));
    private final SelenideElement obszarRealizacjiProjektu = $(byXpath("//div[6]/div/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement option2 = $(byXpath("//mat-option[2]"));
    private final SelenideElement save = $(byXpath("//app-application-project-information-section/div/app-application-section-actions/div/div[2]/button[1]"));
    private final SelenideElement submit = $(byXpath("//app-application-project-information-section/div/app-application-section-actions/div/div[1]/button[1]"));
    private final SelenideElement saveNotification = $(byXpath("//app-notification/div[2]/div/div/p"));
    private final SelenideElement submitNotification = $(byXpath("//app-notification/div[2]/div/div/p"));

    public void editSection() {
        topBar.click();
        editSection.shouldBe(Condition.visible).click();

        dziedzinaProjektu.shouldBe(Condition.visible).click();
        option.shouldBe(Condition.visible).click();


        startDateInput.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        startDateInput.sendKeys("2021-03-12");
        endDateInput.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        endDateInput.sendKeys("2021-03-30");
        projectDescription.shouldBe(Condition.visible).sendKeys("Opis projektu ");
        grupyDocelowe.shouldBe(Condition.visible).sendKeys("Testwoe grupy docelowe");
        obszarRealizacjiProjektu.shouldBe(Condition.visible).click();
        option2.shouldBe(Condition.visible).click();

        save.shouldBe(Condition.visible).click();
        sleep(2000);
        String info1 = saveNotification.shouldBe(Condition.visible).getText();
        System.out.print("1.Sekcja informacje o projekcie. " + info1);
        sleep(2000);
        submit.shouldBe(Condition.visible).click();
        sleep(2000);
        String info2 = submitNotification.shouldBe(Condition.visible).getText();
        System.out.print(info2);
    }

}

