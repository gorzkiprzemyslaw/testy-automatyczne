package SOWA.Sections;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ApplicantAndImplementersPageObject {

    private final SelenideElement topBar = $(byXpath("//mat-tab-group/mat-tab-header/div[2]/div/div/div[2]"));
    private final SelenideElement editSectionButton = $(byXpath("//app-application-organization-information-section/section[1]/div/div/app-application-section-actions/div/div/button"));
    private final SelenideElement applicantSelect = $(byXpath("//div[1]/div[1]/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement applicant = $(byXpath("//mat-option[1]"));
    private final SelenideElement vatList = $(byXpath("//div[4]/div/mat-form-field"));
    private final SelenideElement vatOption = $(byXpath("//mat-option[1]"));
    private final SelenideElement radioButtonYes = $(byXpath("//mat-radio-button[1]"));

    private final SelenideElement addImplementer = $(byXpath("//app-executors-input/button"));
    private final SelenideElement expand = $(byXpath("//div/div[2]/div/div[2]/button[2]"));
    private final SelenideElement organizationType = $(byXpath("//div[2]/div/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement organizationOption = $(byXpath("//mat-option[1]"));
    private final SelenideElement name = $(byXpath("//div[2]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement nip = $(byXpath("//div[3]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement executorType = $(byXpath("//div[4]/app-dictionary-select-view/app-dictionary-select/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement executorTypeOption = $(byXpath("//mat-option[2]"));
    private final SelenideElement propertyForm = $(byXpath("//div[5]/app-dictionary-select-view/app-dictionary-select/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement propertyFormOption = $(byXpath("//mat-option[2]"));
    private final SelenideElement companySize = $(byXpath("//div[6]/app-dictionary-select-view/app-dictionary-select/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement companySizeOption = $(byXpath("//mat-option[2]"));
    private final SelenideElement place = $(byXpath("//div[8]/app-teryt-view/app-teryt/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement placeOption = $(byXpath("//mat-option"));
    private final SelenideElement postCode = $(byXpath("//div[9]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement street = $(byXpath("//div[10]/app-teryt-view/app-teryt/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement streetOption = $(byXpath("//mat-option"));
    private final SelenideElement streetNumber = $(byXpath("//div[11]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement apartmentNumber = $(byXpath("//div[12]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement email = $(byXpath("//div[13]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement phoneNumber = $(byXpath("//div[14]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement website = $(byXpath("//div[15]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement vatRecover = $(byXpath("//div[16]/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement vatRecoverOption = $(byXpath("//mat-option[1]"));

    private final SelenideElement addContact = $(byXpath("//app-contact-persons-input/button"));
    private final SelenideElement nameContact = $(byXpath("//div[1]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement sureName = $(byXpath("//div[1]/div/div[2]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement emailContact = $(byXpath("//div[1]/div/div[3]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement phoneNumberContact = $(byXpath("//div[1]/div/div[4]/mat-form-field/div/div[1]/div/input"));

    private final SelenideElement save = $(byXpath("//app-application-organization-information-section/div/app-application-section-actions/div/div[2]/button[1]"));
    private final SelenideElement submit = $(byXpath("//app-application-organization-information-section/div/app-application-section-actions/div/div[1]/button[1]"));
    private final SelenideElement saveNotification = $(byXpath("//app-notification/div[2]/div/div/p"));
    private final SelenideElement submitNotification = $(byXpath("//app-notification/div[2]/div/div/p"));


    public void editSection (){
        topBar.shouldBe(Condition.visible).click();
        editSectionButton.shouldBe(Condition.visible).click();
        applicantSelect.shouldBe(Condition.visible).click();
        applicant.shouldBe(Condition.visible).click();
        vatList.shouldBe(Condition.visible).click();
        vatOption.shouldBe(Condition.visible).click();
        radioButtonYes.shouldBe(Condition.visible).click();

        addImplementer.shouldBe(Condition.visible).click();
        expand.shouldBe(Condition.visible).click();
        organizationType.shouldBe(Condition.visible).click();
        organizationOption.shouldBe(Condition.visible).click();

        name.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        name.shouldBe(Condition.visible).sendKeys("Name test");

        nip.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        nip.shouldBe(Condition.visible).sendKeys("7398698341");

        executorType.shouldBe(Condition.visible).click();
        executorTypeOption.shouldBe(Condition.visible).click();
        propertyForm.shouldBe(Condition.visible).click();
        propertyFormOption.shouldBe(Condition.visible).click();
        companySize.shouldBe(Condition.visible).click();
        companySizeOption.shouldBe(Condition.visible).click();
        place.shouldBe(Condition.visible).sendKeys("Działdowo");
        sleep(2000);
        placeOption.click();
        postCode.shouldBe(Condition.visible).sendKeys("01-000");
        street.shouldBe(Condition.visible).sendKeys("lidz");
        sleep(2000);
        streetOption.click();

        streetNumber.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        streetNumber.shouldBe(Condition.visible).sendKeys("2");

        apartmentNumber.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        apartmentNumber.shouldBe(Condition.visible).sendKeys("10");

        email.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        email.shouldBe(Condition.visible).sendKeys("test@mail.com");

        phoneNumber.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        phoneNumber.shouldBe(Condition.visible).sendKeys("222222222");

        website.sendKeys((Keys.chord(Keys.CONTROL, "a", Keys.DELETE)));
        website.shouldBe(Condition.visible).sendKeys("www.test.com");
        vatRecover.shouldBe(Condition.visible).click();
        vatRecoverOption.shouldBe(Condition.visible).click();

        addContact.shouldBe(Condition.visible).click();
        nameContact.shouldBe(Condition.visible).sendKeys("TestName");
        sureName.shouldBe(Condition.visible).sendKeys("SureNameTest");
        emailContact.shouldBe(Condition.visible).sendKeys("email@contact.com");
        phoneNumberContact.shouldBe(Condition.visible).sendKeys("333333333");

        save.shouldBe(Condition.visible).click();
        sleep(2000);
        String info1 = saveNotification.shouldBe(Condition.visible).getText();
        System.out.print("Sekcja wnioskodawca i realizatorzy. " + info1);
        sleep(2000);
        submit.shouldBe(Condition.visible).click();
        sleep(2000);
        String info2 = submitNotification.shouldBe(Condition.visible).getText();
        System.out.print(info2);

    }
}
