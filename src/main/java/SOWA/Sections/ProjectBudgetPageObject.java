package SOWA.Sections;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ProjectBudgetPageObject {

    private final SelenideElement topBar = $(byXpath("//mat-tab-group/mat-tab-header/div[2]/div/div/div[5]"));
    private final SelenideElement editSection = $(byXpath("//section[1]/div/app-application-section-actions/div/div/button"));
    private final SelenideElement addCost = $(byXpath("//app-budget-input/div/div[2]/div[4]/button"));
    private final SelenideElement incurredCost = $(byXpath("//div[2]/div/div/div/button[1]"));
    private final SelenideElement expandButton = $(byXpath("//app-budget-input/div/div[3]/div[4]/button[2]"));
    private final SelenideElement costCategorySelect = $(byXpath("//div[4]/div[2]/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement costCategoryOption = $(byXpath("//mat-option[1]"));
    private final SelenideElement costName = $(byXpath("//div[3]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement totalValue = $(byXpath("//div[4]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement fundingAmount = $(byXpath("//div[5]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement executorSelect = $(byXpath("//div[6]/div/div[2]/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement executorOption = $(byXpath("//mat-option[1]"));

    private final SelenideElement save = $(byXpath("//app-application-budget-section/div/app-application-section-actions/div/div[2]/button[1]"));
    private final SelenideElement submit = $(byXpath("//app-application-budget-section/div/app-application-section-actions/div/div[1]/button[1]"));
    private final SelenideElement saveNotification = $(byXpath("//app-notification/div[2]/div/div/p"));
    private final SelenideElement submitNotification = $(byXpath("//app-notification/div[2]/div/div/p"));

    public void editSection (){
        topBar.click();
        editSection.click();
        addCost.click();
        incurredCost.click();
        expandButton.click();
        costCategorySelect.click();
        costCategoryOption.click();
        costName.sendKeys("Test cost");
        totalValue.sendKeys("50000");
        fundingAmount.sendKeys("25000");
        executorSelect.click();
        executorOption.click();

        save.shouldBe(Condition.visible).click();
        sleep(2000);
        String info1 = saveNotification.shouldBe(Condition.visible).getText();
        System.out.print("Sekcja budżet projektu. " + info1);
        sleep(2000);
        submit.shouldBe(Condition.visible).click();
        sleep(2000);
        String info2 = submitNotification.shouldBe(Condition.visible).getText();
        System.out.print(info2);
        sleep(10000);
    }

}
