package SOWA.Sections;

import SOWA.CreateProjectPageObject;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class EditApplicationPageObject {


    private final SelenideElement leftMenuButton = $(byXpath("//div[3]/a[2]"));
    private final SelenideElement searchByNameInput = $(byXpath("//form/div/div[1]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement searchButton = $(byXpath("//app-projects/div/div[2]/div/form/div/div[2]/button"));
    private final SelenideElement hamburger = $(byXpath("//mat-panel-title/button/span[1]/mat-icon"));
    private final SelenideElement editButton = $(byXpath("//div[2]/div/div/div/div/button[1]"));




    public void editApplication() {

        leftMenuButton.click();
        searchByNameInput.sendKeys(CreateProjectPageObject.projectName);
        searchButton.click();
        sleep(3000);
        hamburger.click();
        editButton.shouldBe(Condition.visible).click();
        sleep(5000);

    }
}
