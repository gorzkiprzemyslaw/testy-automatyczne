package SOWA;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;


public class AddRecruitmentPageObject {

    String number;
    private final SelenideElement recruitmentList = $(byXpath("//div/mat-nav-list/div[2]/a[2]"));
    private final SelenideElement addNewRecruitment = $(byXpath("//div/div[2]/div/button"));
    private final SelenideElement choosePWD = $(byXpath("//form/section/div[3]/div/div/app-chose-deployment-level-view"));
    private final SelenideElement inputPWD = $(byXpath("//form/div/div/div/mat-form-field/div/div[1]/div"));
    private final SelenideElement inputInput = $(byXpath("//form/div/div/div/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement search = $(byXpath("//form/div/div/div/button"));
    private final SelenideElement anuluj = $(byXpath("//*/mat-dialog-actions/button[1]"));
    private final SelenideElement PWD = $(byXpath("//ul/mat-tree-node[1]/li"));
    private final SelenideElement choose = $(byXpath("//mat-dialog-actions/button[2]"));
    private final SelenideElement selectApplicantType = $(byXpath("//form/section[2]/div[2]/button"));
    private final SelenideElement selectAll = $(byXpath("//mat-dialog-container/app-checkbox-list-dialog/span"));
    private final SelenideElement selectMarked = $(byXpath("//app-checkbox-list-dialog/mat-dialog-actions/button[2]"));
    private final SelenideElement chooseCompanySize = $(byXpath("//form/section[2]/div[3]/button"));
    private final SelenideElement selectAll2 = $(byXpath("//mat-dialog-container/app-checkbox-list-dialog/span"));
    private final SelenideElement selectMarked2 = $(byXpath("//app-checkbox-list-dialog/mat-dialog-actions/button[2]"));
    private final SelenideElement recruitmentBudget = $(byXpath("//div[1]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement ueBudget = $(byXpath("//div[2]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement description = $(byXpath("//mat-form-field/div/div[1]/div/textarea"));
    private final SelenideElement website = $(byXpath("//div[2]/div[4]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement recruitmentMode = $(byXpath("//div[2]/mat-form-field/div/div[1]/div/mat-select"));
    private final SelenideElement selectRecruitmentMode = $(byXpath("/html/body/div[1]/div[2]/div/div/div/mat-option[1]"));
    private final SelenideElement addRound = $(byXpath("//app-enrollment-schedule-input/div/button"));
    private final SelenideElement startDate = $(byXpath("//div/div/div/div[1]/mat-form-field/div/div[1]/div[1]/input"));
    private final SelenideElement endDate = $(byXpath("//div/div/div/div[2]/mat-form-field/div/div[1]/div[1]/input"));
    private final SelenideElement selectInstytutionEmployees = $(byXpath("//app-choose-supervisors/div[1]/mat-form-field[1]/div/div[1]/div/mat-select"));
    private final SelenideElement instytutionEmployeeCheckbox = $(byXpath("//div/div/div/mat-option[4]"));
    private final SelenideElement hideInstytutionList = $(byXpath("//app-choose-supervisors/div[1]/mat-form-field[1]/div/div[1]/div/mat-select/div/div[2]"));
    private final SelenideElement addAnyEmail = $(byXpath("//div[1]/mat-form-field[2]/div/div[1]/div/input"));
    private final SelenideElement addAnyEmailButton = $(byXpath("//app-choose-supervisors/div[1]/button"));
    private final SelenideElement createNewRecruitment = $(byXpath("//app-new-enrollment/form/div/div/button[2]"));
    private final SelenideElement recruitmentNumber = $(byXpath("//app-table-list/div/div/mat-table/mat-row[1]/mat-cell[2]/div/div/div"));


    public String number() {
        number = recruitmentNumber.getText();
        return number;
    }

    public void recruitmentList() {
        recruitmentList.shouldBe(Condition.visible).click();
    }

    public void addNewRecruitment() {
        addNewRecruitment.shouldBe(Condition.visible).click();
    }

    public void choosePWD() {
        choosePWD.click();
    }

    public void inputPWD() {
        inputPWD.click();
        inputInput.sendKeys("POWR.01.01.01");
        anuluj.click();
        choosePWD.click();
        inputInput.sendKeys("POWR.01.01.01");
    }

    public void search() {
        search.click();
    }

    public void checkboxPWD() {
        PWD.shouldBe(Condition.visible).click();
    }

    public void chooseButton() {
        choose.click();
    }

    public void selectApplicantType() {
        selectApplicantType.click();
        selectAll.click();
        selectMarked.click();
    }

    public void chooseCompanySize() {
        chooseCompanySize.shouldBe(Condition.visible).click();
        selectAll2.click();
        selectMarked2.click();
    }

    public void enterBudget() {
        recruitmentBudget.sendKeys("5000000");
        ueBudget.sendKeys("5000000");
    }

    public void description() {
        description.sendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu urna vehicula, blandit nisl ut, finibus orci.");
    }

    public void setWebsite() {
        website.sendKeys("www.testRecruitment.com.pl");
    }

    public void setRecruitmentMode() {
        recruitmentMode.shouldBe(Condition.visible).click();
        selectRecruitmentMode.shouldBe(Condition.visible).click();
    }

    public void addRound() {
        addRound.click();
    }

    public void setStartDate() {
        startDate.sendKeys("2021.02.12");
    }

    public void setEndDate() {
        endDate.sendKeys("2021.08.01");
    }

    //public void selectInstytutionEmployees (){
    //selectInstytutionEmployees.click();
    // instytutionEmployeeCheckbox.click();
    // hideInstytutionList.click();

    // }

    public void addAnyEmail() {
        addAnyEmail.sendKeys("test@email.com");
        addAnyEmailButton.click();
    }

    public void createNewRecruitment() {
        createNewRecruitment.click();
    }

    public void setRecruitmentNumber() {
        number = recruitmentNumber.shouldBe(Condition.visible).getText();
        System.out.print("Test result recruitment number:");
        System.out.print(number);
    }


}
