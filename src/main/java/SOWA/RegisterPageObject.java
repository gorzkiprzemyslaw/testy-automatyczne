package SOWA;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class RegisterPageObject {

    private final SelenideElement emailInput = $(byId("mat-input-0"));
    private final SelenideElement repeatEmailInput = $(byId("mat-input-1"));
    private final SelenideElement passwordInput = $(byId("mat-input-2"));
    private final SelenideElement repeatPasswordInput = $(byId("mat-input-3"));
    private final SelenideElement nameInput = $(byId("mat-input-4"));
    private final SelenideElement surnameInput = $(byId("mat-input-5"));
    private final SelenideElement peselInput = $(byId("mat-input-6"));
    private final SelenideElement phoneInput = $(byId("mat-input-7"));
    private final SelenideElement regulations = $(By.cssSelector("div.ng-star-inserted a.text-decoration-underline"));
    private final SelenideElement regulationsScroll = $(byXpath("//*/app-term-dialog/div/br[93]"));
    private final SelenideElement acceptRegulations = $(byXpath("//*/app-term-dialog/div[3]/button[2]"));
    private final SelenideElement registerButton = $(byXpath("//div[4]/div/button"));

    public void inputEmail(String email) {
        emailInput.sendKeys(email);
    }

    public void repeatEmailInput(String email) {
        repeatEmailInput.sendKeys(email);
    }

    public void passwordInput(String password) {
        passwordInput.sendKeys(password);
    }

    public void repeatPasswordInput(String password) {
        repeatPasswordInput.sendKeys(password);
    }

    public void nameInput(String name) {
        nameInput.sendKeys(name);
    }

    public void sureNameInput(String surename) {
        surnameInput.sendKeys(surename);
    }

    public void peselInput(String pesel) {
        peselInput.sendKeys(pesel);
    }

    public void phoneInput(String phone) {
        phoneInput.sendKeys(phone);
    }

    public void clickRegulations() {
        regulations.click();
    }

    public void scrollRegulations() {
        regulationsScroll.scrollIntoView(true);
    }

    public void acceptRegulations() {
        acceptRegulations.click();
    }

    public void register(){
        registerButton.click();
    }


}
