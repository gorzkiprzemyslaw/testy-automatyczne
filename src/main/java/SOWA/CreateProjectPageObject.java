package SOWA;

import com.codeborne.selenide.SelenideElement;
import java.util.Date;

import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class CreateProjectPageObject {


    public static String projectName = ("Project name48");
    private final SelenideElement leftMenuButton = $(byXpath("//div[3]/a[2]"));
    private final SelenideElement addNewProjectButton = $(byXpath("//div[1]/div[2]/div[1]/button"));
    private final SelenideElement inputRecruitmentNubmer = $(byXpath("//app-choose-enrollment-dialog/form/div[1]/div/mat-form-field/div/div[1]/div/input"));
    private final String recruitmentNubmeer = "POWR.01.01.01-IP.12-036/21";
    private final SelenideElement nextButton = $(byXpath("//app-choose-enrollment-dialog/form/div[2]/div/button[2]"));
    private final SelenideElement inputProjectName =$(byXpath("//mat-form-field/div/div[1]/div/textarea"));
    private final SelenideElement createButton = $(byXpath("//app-create-project-dialog/form/div[2]/div/button[2]"));
    private final SelenideElement notification = $(byXpath("//app-notification/div[2]/div/div/p"));



    public void createProject (){
        leftMenuButton.click();
        addNewProjectButton.click();
        inputRecruitmentNubmer.sendKeys(recruitmentNubmeer);
        nextButton.click();
        inputProjectName.sendKeys(projectName);
        createButton.click();
        sleep(3000);
        String info = notification.getText();
        System.out.print(info);
    }
    public String projectName(){
        return projectName;
    }




}
