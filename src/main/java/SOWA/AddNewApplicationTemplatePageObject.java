package SOWA;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import java.util.Random;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class AddNewApplicationTemplatePageObject {

    private final SelenideElement leftMenuApplicationTemplate = $(byXpath("//app-main-menu-view/div/mat-nav-list/div[9]/a[2]"));
    private final SelenideElement addNewTemplate = $(byXpath("//app-document-templates/div/div[1]/button"));
    private final SelenideElement choosePwd = $(byXpath("//app-chose-deployment-level/button"));
    private final SelenideElement inputPwd = $(byXpath("//mat-form-field/div/div[1]/div/input"));
    private final SelenideElement search = $(byXpath("//app-tree-view/app-tree/form/div/div/div/button"));
    private final SelenideElement cancel = $(byXpath("//*/mat-dialog-actions/button[1]"));
    private final SelenideElement PWD = $(byXpath("//ul/mat-tree-node[1]/li"));
    private final SelenideElement choose = $(byXpath("//mat-dialog-actions/button[2]"));
    private final SelenideElement applicationName = $(byXpath("//mat-form-field/div/div[1]/div/textarea"));
    private final SelenideElement declaration = $(byXpath("//div[2]/div/div/div[3]"));
    private final SelenideElement declarationContent = $(byXpath("//mat-quill/div[2]/div[1]"));
    private final SelenideElement checkBox1 = $(byXpath("//section[2]/div/div/div/div[1]/mat-checkbox[1]"));
    private final SelenideElement checkBox2 = $(byXpath("//section[2]/div/div/div/div[1]/mat-checkbox[2]"));
    private final SelenideElement checkBox3 = $(byXpath("//section[2]/div/div/div/div[1]/mat-checkbox[3]"));
    private final SelenideElement add = $(byXpath("//form/div[2]/div/button[2]"));
    private final SelenideElement searchByName = $(byXpath("//form/div/div[1]/mat-form-field/div/div[1]/div/input"));
    private final SelenideElement searchApplication = $(byXpath("//form/div/div[2]/button"));
    private final SelenideElement hamburger = $(byXpath("//div[1]/mat-card/mat-card-header/button"));
    private final SelenideElement accept = $(byXpath("//div/div/div/div/button[3]"));

    Random random = new Random();
    int rand = random.nextInt(100);
    private final String name  =("Test name" + rand);
    
    public void fillForm (){
        leftMenuApplicationTemplate.shouldBe(Condition.visible).click();
        addNewTemplate.shouldBe(Condition.visible).click();

        choosePwd.shouldBe(Condition.visible).click();
        String powr = "POWR.01.01.01";
        inputPwd.shouldBe(Condition.visible).sendKeys(powr);
        search.click();
        cancel.click();
        choosePwd.click();
        inputPwd.sendKeys(powr);
        search.click();
        PWD.click();
        choose.click();

        applicationName.sendKeys(name);

        declaration.shouldBe(Condition.visible).click();
        declarationContent.sendKeys("Declaration content");
        checkBox1.click();
        checkBox2.click();
        checkBox3.click();
        add.click();

        searchByName.sendKeys(name);
        searchApplication.click();
        sleep(5000);
        hamburger.shouldBe(Condition.visible).click();
        accept.shouldBe(Condition.visible).click();

    }

}
