package SOWA;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ConfirmRecruitmentPageObject {

    private final SelenideElement leftMenuButton = $(byXpath("//app-main-menu-view/div/mat-nav-list/div[2]/a[2]/div"));
    private final SelenideElement recuitment = $(byXpath("//mat-table/mat-row[1]/mat-cell[1]/div/div/div"));
    private final SelenideElement addTemplate = $(byXpath("/html/body/div[1]/div[2]/div/div/div/div/button[6]"));
    private final SelenideElement radioButton = $(byXpath("//form/div[1]/div/mat-radio-group/mat-radio-button[1]"));
    private final SelenideElement saveButton = $(byXpath("//form/div[2]/div/div/button[3]"));
    private final SelenideElement positivePopUp = $(byXpath("//app-notifications/div/div/app-notification/div[2]/div/div/p"));
    private final SelenideElement confirmRecruitmentButton = $(byXpath("/html/body/div[1]/div[2]/div/div/div/div/button[9]"));
    private final SelenideElement positivePopUp2 = $(byXpath("//app-notifications/div/div/app-notification/div[2]/div/div/p"));
    String positive;
    String positive2;

    public void addApplicationToRecruitment() {
        leftMenuButton.shouldBe(Condition.visible).click();
        recuitment.shouldBe(Condition.visible).click();
        addTemplate.shouldBe(Condition.visible).click();
        radioButton.shouldBe(Condition.visible).click();
        saveButton.shouldBe(Condition.visible).click();
        positive = positivePopUp.getText();
        System.out.print(positive);
    }

    public void confirmRecruitment() {
        recuitment.shouldBe(Condition.visible).click();
        confirmRecruitmentButton.shouldBe(Condition.visible).click();
        sleep(3000);
        positive2 = positivePopUp2.getText();
        System.out.print(positive2);
    }
}
